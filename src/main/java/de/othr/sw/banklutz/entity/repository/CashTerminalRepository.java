package de.othr.sw.banklutz.entity.repository;

import de.othr.sw.banklutz.entity.CashTerminal;
import de.othr.sw.banklutz.entity.util.SingleIdEntityRepository;
import javax.enterprise.context.RequestScoped;

@RequestScoped
public class CashTerminalRepository extends SingleIdEntityRepository<Long, CashTerminal> {
    
}
