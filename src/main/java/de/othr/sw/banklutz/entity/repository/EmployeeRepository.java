package de.othr.sw.banklutz.entity.repository;

import de.othr.sw.banklutz.entity.Employee;
import de.othr.sw.banklutz.entity.util.StringIdEntityRepository;
import javax.enterprise.context.RequestScoped;

@RequestScoped
public class EmployeeRepository extends StringIdEntityRepository<Employee> {
    
}
