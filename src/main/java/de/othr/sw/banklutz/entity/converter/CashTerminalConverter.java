package de.othr.sw.banklutz.entity.converter;

import de.othr.sw.banklutz.entity.CashTerminal;
import de.othr.sw.banklutz.entity.repository.CashTerminalRepository;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;

@RequestScoped
public class CashTerminalConverter implements Converter {
    @Inject
    private CashTerminalRepository repo;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(value == null)
            return "";
        
        CashTerminal terminal = repo.getById(Long.valueOf(value));
        
        if(terminal == null)
            return "";
        
        return terminal;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value == null)
            return null;
        
        if(!value.getClass().equals(CashTerminal.class))
            return null;
        
        Long id = ((CashTerminal) value).getId();
        
        return String.valueOf(id);
    }
    
}
