package de.othr.sw.banklutz.entity;

import de.othr.sw.banklutz.entity.util.SingleIdEntity;
import de.othr.sw.banklutz.exception.CreditOutOfLimitException;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CashTerminal extends SingleIdEntity<Long> implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private int storedCash = 0;
    private String name;
    
    public CashTerminal() { }
    
    public CashTerminal(String name, int cash) {
        this.name = name;
        storedCash = cash;
    }
    
    @Override
    public Long getId() {
        return id;
    }
    
    public void setStoredCash(int cash) {
        storedCash = cash;
    }

    public int getStoredCash() {
        return storedCash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addCash(int amount) {
        storedCash += amount;
    }
    
    public void reduceCash(int amount) throws CreditOutOfLimitException {
        if(storedCash - amount < 0) 
            throw new CreditOutOfLimitException();
        
        storedCash -= amount;
    }
    
    public boolean hasCash() {
        return storedCash > 0;
    }

    @Override
    public String toString() {
        return name;
    }
}
