package de.othr.sw.banklutz.entity.converter;

import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.repository.AccountRepository;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;

@RequestScoped
public class AccountConverter implements Converter {
    @Inject
    private AccountRepository repo;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(value == null)
            return "";
        
        Account account = repo.getById(Long.valueOf(value));
        
        if(account == null)
            return "";
        
        return account;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value == null)
            return null;
        
        if(!value.getClass().equals(Account.class))
            return null;
        
        Long id = ((Account) value).getId();
        
        return String.valueOf(id);
    }
    
}
