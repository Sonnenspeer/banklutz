package de.othr.sw.banklutz.entity.repository;

import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.util.SingleIdEntityRepository;
import javax.enterprise.context.RequestScoped;

@RequestScoped
public class AccountRepository extends SingleIdEntityRepository<Long, Account> {

}
