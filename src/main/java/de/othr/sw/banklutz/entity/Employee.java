package de.othr.sw.banklutz.entity;

import javax.persistence.Entity;

@Entity
public class Employee extends User {
    public Employee() {
        super();
    }
    
    public Employee(String email, String password) {
        super(email, password);
    }
}
