package de.othr.sw.banklutz.entity;

import de.othr.sw.banklutz.entity.embeddable.Address;
import de.othr.sw.banklutz.entity.util.EntityUtils;
import de.othr.sw.banklutz.entity.util.StringIdEntity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class User extends StringIdEntity implements Serializable {
    public static final String HASH_ALGORITHM = "SHA-512";
    
    private String password;
    private String salt;
    
    private String name;
    private String lastName;
    private Address address;

    public User() {
        super();
    }
    
    public User(String email, String passwort) {
        super(email);
        
        salt = EntityUtils.createRandomString(4);
        
        try {
            password = EntityUtils.hashPassword(passwort, this.salt, HASH_ALGORITHM);
        } catch (EntityUtils.EntityUtilException ex) {
            throw new RuntimeException("password can not be hashed", ex);
        }
    }
        
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }    
}
