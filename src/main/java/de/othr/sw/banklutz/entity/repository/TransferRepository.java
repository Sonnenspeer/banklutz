package de.othr.sw.banklutz.entity.repository;

import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.Customer;
import de.othr.sw.banklutz.entity.Transfer;
import de.othr.sw.banklutz.entity.util.SingleIdEntityRepository;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.persistence.TypedQuery;

@RequestScoped
public class TransferRepository extends SingleIdEntityRepository<Long, Transfer> {
    public List<Transfer> getBySender(Account sender){
        TypedQuery<Transfer> query = getEntityManager().createQuery("SELECT t FROM Transfer t LEFT JOIN t.sender s "
                + "WHERE s.id = :senderId", Transfer.class);
        query.setParameter("senderId", sender.getId());   
        return query.getResultList();
    }
    
    public List<Transfer> getByReciever(Account reciever){
        TypedQuery<Transfer> query = getEntityManager().createQuery("SELECT t FROM Transfer t LEFT JOIN t.reciever r "
                + "WHERE r.id = :recieverId", Transfer.class);
        query.setParameter("recieverId", reciever.getId());
        return query.getResultList();
    }
    
    public List<Transfer> getTransfersByAccount(Account account){
        TypedQuery<Transfer> query = getEntityManager().createQuery("SELECT t FROM Transfer t "
                + "WHERE t.reciever.id = :recieverId OR t.sender.id = :senderId", Transfer.class);
        query.setParameter("recieverId", account.getId());
        query.setParameter("senderId", account.getId());  
        return query.getResultList();
    }
}
