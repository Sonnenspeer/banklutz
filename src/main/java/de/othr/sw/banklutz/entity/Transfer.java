package de.othr.sw.banklutz.entity;

import de.othr.sw.banklutz.entity.util.SingleIdEntity;
import de.othr.sw.banklutz.exception.CreditOutOfLimitException;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.xml.bind.annotation.XmlType;

@Entity
@XmlType(namespace="de.othr.sw.banklutz.entity")
public class Transfer extends SingleIdEntity<Long> implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private Account sender;
    
    @ManyToOne
    private Account reciever;
    
    @Min(1)
    private int amount;
    
    @Temporal(value=TemporalType.TIMESTAMP)
    private Date time = new Date();
    
    public Transfer() {
        
    }
    
    public Transfer(Account sender, Account reciever, int amount) throws CreditOutOfLimitException {
        this.sender = sender;
        this.reciever = reciever;
        this.amount = amount;
        
        exchange();
    }
    
    private void exchange() throws CreditOutOfLimitException {
        sender.reduceCredits(amount);
        reciever.addCredits(amount);
    }

    @Override
    public Long getId() {
        return id;
    }

    public Account getSender() {
        return sender;
    }
    
    public void setSender(Account sender) {
        this.sender = sender;
    }

    public Account getReciever() {
        return reciever;
    }
    
    public void setReciever(Account reciever) {
        this.reciever = reciever;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getTime() {
        return time;
    }
}
