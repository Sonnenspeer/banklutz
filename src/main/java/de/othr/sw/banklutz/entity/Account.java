package de.othr.sw.banklutz.entity;

import de.othr.sw.banklutz.entity.util.SingleIdEntity;
import de.othr.sw.banklutz.exception.CreditOutOfLimitException;
import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue(value="Account")
public class Account extends SingleIdEntity<Long> implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private Customer customer;
    
    /**
     * Credits specified in cents
     * 10 Euro by default
     */
    protected int credits = 1000;
    
    @NotNull
    private int pin;
    
    public Account() { }
    
    public Account(int pin) {
        this.pin = pin;
    }

    /**
     * Account id
     * 
     * @return Long
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * 
     * @return int
     */
    public int getCredits() {
        return credits;
    }

    /**
     * Add credits in cents
     * 
     * @param credits 
     */
    public void addCredits(int credits) {
        this.credits += credits;
    }
    
    /**
     * Reduce credits in cents
     * 
     * @param credits
     * @throws CreditOutOfLimitException 
     */
    public void reduceCredits(int credits) throws CreditOutOfLimitException {
        if((this.credits - credits) < 0)
            throw new CreditOutOfLimitException();
        
        this.credits -= credits;  
    }

    /**
     * 
     * @return int
     */
    public int getPin() {
        return pin;
    }

    /**
     * 
     * @param pin 
     */
    public void setPin(int pin) {
        this.pin = pin;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Kontonr. " + Long.toString(id);
    }        
}
