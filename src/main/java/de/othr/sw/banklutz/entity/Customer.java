package de.othr.sw.banklutz.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class Customer extends User {
    @OneToMany(cascade=CascadeType.REMOVE, mappedBy="customer", targetEntity=Account.class, fetch = FetchType.EAGER)
    private List<Account> accounts;
    
    public Customer() {
        super();
        accounts = new ArrayList<>();
    }
    
    public Customer(String email, String password) {
        super(email, password);
        accounts = new ArrayList<>();
    }

    public void addAccount(Account account) {
        if(account != null) {
            if(!accounts.contains(account)) {
                accounts.add(account);
                if(account.getCustomer() == null) 
                    account.setCustomer(this);
            }        
        } 
    }
    
    public void removeAccount(Account account) {
        if(account != null) {
            if(accounts.contains(account)) {
                accounts.remove(account);  
                if(account.getCustomer() != null) {
                    account.setCustomer(null);
                }
            }     
        } 
    }
    
    public List<Account> getModifiableAccounts() {           
        return accounts;
    }
    
    public List<Account> getAccounts() {
        return Collections.unmodifiableList(accounts);
    }
}
