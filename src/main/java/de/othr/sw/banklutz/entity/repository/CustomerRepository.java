package de.othr.sw.banklutz.entity.repository;

import de.othr.sw.banklutz.entity.Customer;
import de.othr.sw.banklutz.entity.util.StringIdEntityRepository;
import javax.enterprise.context.RequestScoped;

@RequestScoped
public class CustomerRepository extends StringIdEntityRepository<Customer> {

}
