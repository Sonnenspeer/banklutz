package de.othr.sw.banklutz.entity;

import de.othr.sw.banklutz.exception.CreditOutOfLimitException;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="GiroAccount")
public class GiroAccount extends Account {
    /**
     * Debit limit in cents
     */
    protected static int DEBIT_LIMIT = 100000;
    
    public GiroAccount() { }
    
    public GiroAccount(int pin) {
        super(pin);
    }

    /**
     * Reduces credits with debit limit tracking
     * 
     * @param credits
     * @throws CreditOutOfLimitException 
     */
    @Override
    public void reduceCredits(int credits) throws CreditOutOfLimitException {
        if((this.credits - credits) < DEBIT_LIMIT * -1 )
            throw new CreditOutOfLimitException();
        
        this.credits -= credits;
    }
    
    /**
     * Returns debit
     * 
     * @return int
     */
    public int getDebit() {
        return (credits < 0) ? credits * -1 : 0;
    }
}
