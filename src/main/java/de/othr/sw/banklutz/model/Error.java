package de.othr.sw.banklutz.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class Error implements Serializable {
    private List<String> errors = new LinkedList<>();
    
    public void addError(String e) {
        errors.add(e);
    }
    
    public List<String> getErrors() {
        return errors;
    }
    
    public void clear() {
        errors.clear();
    }
}
