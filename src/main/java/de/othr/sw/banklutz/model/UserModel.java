package de.othr.sw.banklutz.model;

import de.othr.sw.banklutz.service.AccountService;
import de.othr.sw.banklutz.service.FixtureService;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

@SessionScoped
public abstract class UserModel implements Serializable {    
    @Inject 
    protected AccountService accountService;
    
    @Inject
    protected FixtureService fixtureService;
    
    @Inject
    protected Error error;
    
    protected String email;
    protected String password;
    
    protected String name;
    protected String lastName;
    protected String street;
    protected String postcode;
    protected String place;
    
    @PostConstruct
    public void generateFixtures() {
        fixtureService.generateFixtures();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }    
}
