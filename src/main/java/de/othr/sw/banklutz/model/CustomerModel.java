package de.othr.sw.banklutz.model;

import de.othr.sw.banklutz.annotation.CustomerBean;
import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.Customer;
import de.othr.sw.banklutz.entity.embeddable.Address;
import de.othr.sw.banklutz.service.CustomerService;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped 
@CustomerBean
public class CustomerModel extends UserModel {
    @Inject @CustomerBean
    private CustomerService customerService;
    
    private int pin;
    
    private Customer userInSession = null;
    private Customer targetCustomer;
    
    public String register() {
        Customer customer = new Customer(email, password);
        customer.setName(name);
        customer.setLastName(lastName);
        customer.setAddress(new Address(street, postcode, place));
        
        Account account = new Account(pin);
        
        try {
            customerService.createCustomerWithAccount(customer, account);
        } catch(Exception e) {
            error.addError("Kunde konnte nicht erstellt werden. Bitte überprüfen Sie alle Daten.");
            return "";
        }
        
        return "index";
    }
    
    public void updateCurrentUser() {        
        userInSession = customerService.updateCustomer(userInSession);
    }
    
    public String login() {
        userInSession = customerService.checkUserPassword(email, password);
        
        if(userInSession != null) {
            return "dashboard/overview";
        }
        
        error.addError("Benutzer konnte nicht eingeloggt werden. Bitte überprüfen Sie die Daten.");
        return "index";
    }
    
    public String logout() {
        userInSession = null;
        return "/index";
    }
    
    public String edit(String id) {
        targetCustomer = customerService.getCustomerById(id);
        return "edit";
    }
    
    public String update() {
        if(targetCustomer != null) {
            customerService.updateCustomer(targetCustomer);
            targetCustomer = null;
        }
        
        return "overview";
    }
    
    public String delete(String id) {
        Customer customer = customerService.getCustomerById(id);
        
        if(customer != null)
            customerService.deleteCustomer(customer);
        
        return "overview";
    }
    
    public boolean hasActiveUser() {
        if(userInSession != null)
            return true;
        else
            return false;
    }
    
    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }
    
    public Customer getUserInSession() {
        return userInSession;
    }

    public Customer getTargetCustomer() {
        return targetCustomer;
    }
    
    public List<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }
}
