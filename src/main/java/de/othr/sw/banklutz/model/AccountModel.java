package de.othr.sw.banklutz.model;

import de.othr.sw.banklutz.service.AccountService;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class AccountModel implements Serializable {
    @Inject
    private AccountService accountService;
}
