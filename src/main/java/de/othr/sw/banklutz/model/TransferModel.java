package de.othr.sw.banklutz.model;

import de.othr.sw.banklutz.annotation.CustomerBean;
import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.Transfer;
import de.othr.sw.banklutz.entity.converter.AccountConverter;
import de.othr.sw.banklutz.exception.CreditOutOfLimitException;
import de.othr.sw.banklutz.log.Log;
import de.othr.sw.banklutz.service.AccountService;
import de.othr.sw.banklutz.service.TransferService;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class TransferModel implements Serializable {
    @Inject
    private TransferService transferService;
    
    @Inject
    private AccountService accountService; 
    
    @Inject
    @CustomerBean
    private CustomerModel customerModel;
    
    @Inject
    private Log log;
    
    @Inject
    private Error error;
    
    @Inject
    private AccountConverter accountConverter;

    private Long senderId;
    private Long recieverId;
    private int amount;
    
    private int highAmount;
    private int lowAmount;
    
    private Account selectedAccount;
    private List<Account> customerAccounts;
    
    private List<Transfer> transfers;
    
    @PostConstruct
    private void init() {
        customerAccounts = customerModel.getUserInSession().getAccounts();
        
        transfers = new LinkedList<>();
    }
    
    public String transfer() {
        amount = calculateAmount();
        
        try {
            transferService.transfer(senderId, recieverId, amount);
        } catch (CreditOutOfLimitException ex) {
            error.addError("Geld konnte nicht überwiesen werden. Es ist nicht genug Guthaben auf dem Konto.");
            log.logger().log(Level.SEVERE, "Transfer from Account " + senderId + " to " + recieverId + " failed");
            return "transfer";
        } catch(Exception e) {
            error.addError("Geld konnte nicht überwiesen werden. Ein Fehler bei der Verarbeitung ist entstanden.");
            return "transfer";
        }
        
        return "overview";
    }
    
    public String selectAccount() {
        if(selectedAccount != null)
            transfers = transferService.getTransfersByAccount(selectedAccount);
      
        return "overview";
    }

    public List<Transfer> getTransfers() {
        return transfers;
    }
    
    public int calculateAmount() {
        return (highAmount * 100) + lowAmount;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getRecieverId() {
        return recieverId;
    }

    public void setRecieverId(Long recieverId) {
        this.recieverId = recieverId;
    }

    public int getHighAmount() {
        return highAmount;
    }

    public void setHighAmount(int highAmount) {
        this.highAmount = highAmount;
    }

    public int getLowAmount() {
        return lowAmount;
    }

    public void setLowAmount(int lowAmount) {
        this.lowAmount = lowAmount;
    }

    public Account getSelectedAccount() {
        return selectedAccount;
    }

    public void setSelectedAccount(Account selectedAccount) {
        this.selectedAccount = selectedAccount;
    }

    public List<Account> getCustomerAccounts() {
        return customerAccounts;
    }
    
    public List<Long> getCustomerAccountIds() {
        List<Long> ids = new LinkedList<>();
        for(Account a : getCustomerAccounts()) {
            ids.add(a.getId());
        }
        return ids;
    }

    public AccountConverter getAccountConverter() {
        return accountConverter;
    }
}
