package de.othr.sw.banklutz.model;

import de.othr.sw.banklutz.service.DeliveryService;
import de.othr.sw.lieferservice_roesch.service.Adresse;
import de.othr.sw.lieferservice_roesch.service.Auftrag;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class DeliveryModel {
    @Inject
    private DeliveryService deliveryService;
    
    @Inject 
    private Error error;
    
    private int cent;
    private String fromStreet;
    private String fromPostcode;
    private String fromPlace;
    private String toStreet;
    private String toPostcode;
    private String toPlace;
    
    public String createDelivery() {
        Adresse from = new Adresse();
        from.setStraße(fromStreet);
        from.setPlz(fromPostcode);
        from.setOrt(fromPlace);
        
        Adresse to = new Adresse();
        to.setStraße(toStreet);
        to.setPlz(toPostcode);
        to.setOrt(toPlace);
        
        if(deliveryService.createDelivery(cent, from, to) == null) {
            error.addError("Geldlieferung konnte nicht angefordert werden. Es scheint ein Problem mit der Verarbeitung zu geben. Bitte versuchen Sie es später erneut.");
            return "request";
        }
        
        return "overview";
    }
    
    public List<Auftrag> getAllDeliveries() {
        return deliveryService.getDeliveries();
    }

    public int getCent() {
        return cent;
    }

    public void setCent(int cent) {
        this.cent = cent;
    }

    public String getFromStreet() {
        return fromStreet;
    }

    public void setFromStreet(String fromStreet) {
        this.fromStreet = fromStreet;
    }

    public String getFromPostcode() {
        return fromPostcode;
    }

    public void setFromPostcode(String fromPostcode) {
        this.fromPostcode = fromPostcode;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    public String getToStreet() {
        return toStreet;
    }

    public void setToStreet(String toStreet) {
        this.toStreet = toStreet;
    }

    public String getToPostcode() {
        return toPostcode;
    }

    public void setToPostcode(String toPostcode) {
        this.toPostcode = toPostcode;
    }

    public String getToPlace() {
        return toPlace;
    }

    public void setToPlace(String toPlace) {
        this.toPlace = toPlace;
    }
}
