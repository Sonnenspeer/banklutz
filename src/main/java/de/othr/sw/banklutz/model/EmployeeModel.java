package de.othr.sw.banklutz.model;

import de.othr.sw.banklutz.annotation.EmployeeBean;
import de.othr.sw.banklutz.entity.Employee;
import de.othr.sw.banklutz.entity.embeddable.Address;
import de.othr.sw.banklutz.service.EmployeeService;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
@EmployeeBean
public class EmployeeModel extends UserModel {
    @Inject @EmployeeBean
    private EmployeeService employeeService;
    
    private Employee userInSession = null;
    
    private Employee targetEmployee;
    
    public String login() {
        userInSession = employeeService.checkUserPassword(email, password);
        
        if(userInSession != null) {
            return "overview";
        }
        
        error.addError("Benutzer konnte nicht eingeloggt werden. Bitte überprüfen Sie die Daten.");
        return "login";
    }
    
    public String logout() {
        userInSession = null;
        return "/index";
    }
    
    public void updateCurrentUser() {        
        userInSession = employeeService.updateEmployee(userInSession);
    }
    
    public String create() {
        Employee employee = new Employee(email, password);
        employee.setName(name);
        employee.setLastName(lastName);
        employee.setAddress(new Address(street, postcode, place));
        
        try {
            employeeService.createEmployee(employee);
        } catch(Exception e) {
            error.addError("Mitarbeiter konnte nicht erstellt werden. Bitte überprüfen Sie alle Daten.");
            return "";
        }
        
        return "overview";
    }
    
    public String edit(String id) {
        targetEmployee = employeeService.getEmployeeById(id);
        return "edit";
    }
    
    public String update() {
        if(targetEmployee != null) {
            employeeService.updateEmployee(targetEmployee);
            targetEmployee = null;
        }
        
        return "overview";
    }
    
    public String delete(String id) {
        Employee employee = employeeService.getEmployeeById(id);

        if(employee != null)
            employeeService.deleteEmployee(employee); 
        
        return "overview";
    }
    
    public boolean hasActiveUser() {
        if(userInSession != null)
            return true;
        else
            return false;
    }
    
    public Employee getUserInSession() {
        return userInSession;
    }

    public Employee getTargetEmployee() {
        return targetEmployee;
    }
    
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }
}
