package de.othr.sw.banklutz.model;

import de.othr.sw.banklutz.annotation.CustomerBean;
import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.CashTerminal;
import de.othr.sw.banklutz.entity.converter.CashTerminalConverter;
import de.othr.sw.banklutz.exception.CreditOutOfLimitException;
import de.othr.sw.banklutz.service.AccountService;
import de.othr.sw.banklutz.service.CashTerminalService;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class CashTerminalModel implements Serializable {  
    @Inject
    private CashTerminalService terminalService;
    
    @Inject
    private AccountService accountService;
    
    @Inject @CustomerBean
    private CustomerModel customerModel;
    
    @Inject
    private Error error;
    
    @Inject
    private CashTerminalConverter cashTerminalConverter;
    
    private long accountId;
    private int pin;
    private int lowAmount;
    private int highAmount;
    private int amount;
    private int storedCash;
    private String name;
    
    private CashTerminal terminal;
    
    private CashTerminal targetTerminal;
    
    public String deposit() {
        Account account = accountService.getAccountById(accountId);
        
        amount = (highAmount * 100) + lowAmount;
        
        if(checkPin(account, pin)) {
            try {
                terminalService.deposit(terminal, account, amount);
                customerModel.updateCurrentUser();
            } catch (CreditOutOfLimitException ex) {
                error.addError("Einzahlung nicht erfolgreich. Guthaben im Automaten nicht ausreichend.");
                return "";
            }
            
            return "overview";
        }
        
        return "";
    }
    
    public String withdraw() {
        Account account = accountService.getAccountById(accountId);
        
        amount = (highAmount * 100) + lowAmount;
        
        if(checkPin(account, pin)) {
            try {
                terminalService.withdraw(terminal, account, amount);
                customerModel.updateCurrentUser();
            } catch (CreditOutOfLimitException ex) {
                error.addError("Auszahlung nicht erfolgreich. Guthaben im Konto nicht ausreichend.");
                return "";
            }
            
            return "overview";
        }
        
        return "";
    }
    
    public String create() {
        CashTerminal terminal = new CashTerminal(name, storedCash);
        
        terminalService.createCashTerminal(terminal);
        
        return "overview";
    }
    
    public String edit(Long id) {
        targetTerminal = terminalService.getTerminalById(id);
        return "edit";
    }
    
    public String update() {
        if(targetTerminal != null) {
            terminalService.updateCashTerminal(targetTerminal);
            targetTerminal = null;
        }
        
        return "overview";
    }
    
    public String delete(Long id) {
        CashTerminal terminal = terminalService.getTerminalById(id);
        
        if(terminal != null)
            terminalService.deleteCashTerminal(terminal);
        
        return "overview";
    }

    public List<CashTerminal> getTerminals() {
        return terminalService.getAllTerminals();
    }
    
    public List<Account> getCustomerAccounts() {
        return customerModel.getUserInSession().getAccounts();
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public int getLowAmount() {
        return lowAmount;
    }

    public void setLowAmount(int lowAmount) {
        this.lowAmount = lowAmount;
    }

    public int getHighAmount() {
        return highAmount;
    }

    public void setHighAmount(int highAmount) {
        this.highAmount = highAmount;
    }

    public int getStoredCash() {
        return storedCash;
    }

    public void setStoredCash(int storedCash) {
        this.storedCash = storedCash;
    }

    public CashTerminal getTerminal() {
        return terminal;
    }

    public void setTerminal(CashTerminal terminal) {
        this.terminal = terminal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CashTerminal getTargetTerminal() {
        return targetTerminal;
    }
    
    private boolean checkPin(Account account, int pin) {
        return accountService.checkPin(account, pin);
    }

    public CashTerminalConverter getCashTerminalConverter() {
        return cashTerminalConverter;
    }
    
}
