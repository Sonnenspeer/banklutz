package de.othr.sw.banklutz.exception;

/**
 * Exception for credit limit
 */
public class CreditOutOfLimitException extends Exception { }
