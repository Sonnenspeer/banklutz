package de.othr.sw.banklutz.bootsfaces;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@SessionScoped
public class BootsFaces implements Serializable {
    public static final String DEFAULT_THEME = "default";
    public static final String EMPLOYEE_THEME = "simplex";
    
    public String currentTheme() {
        FacesContext context = FacesContext.getCurrentInstance();
        String viewId = context.getViewRoot().getViewId();
        
        if(viewId.startsWith("/employee") && !viewId.endsWith("login.xhtml"))
            return EMPLOYEE_THEME;
        
        return DEFAULT_THEME;
    }
}
