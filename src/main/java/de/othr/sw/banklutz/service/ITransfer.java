package de.othr.sw.banklutz.service;

import de.othr.sw.banklutz.entity.Transfer;
import de.othr.sw.banklutz.exception.CreditOutOfLimitException;

public interface ITransfer {
    public Transfer transfer(Long senderId, Long recieverId, int amount) throws CreditOutOfLimitException;
}
