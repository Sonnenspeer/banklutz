package de.othr.sw.banklutz.service;

import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.Transfer;
import de.othr.sw.banklutz.entity.repository.TransferRepository;
import de.othr.sw.banklutz.exception.CreditOutOfLimitException;
import de.othr.sw.banklutz.log.Log;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.transaction.Transactional;

@WebService
@RequestScoped
public class TransferService implements ITransfer, Serializable {
    @Inject
    private TransferRepository repo;
    
    @Inject
    private AccountService accountService;
    
    @Inject
    private Log log;

    @Override
    @Transactional
    @WebMethod
    public Transfer transfer(@WebParam Long senderId, @WebParam Long recieverId,  @WebParam int amount) throws CreditOutOfLimitException {
        Account sender = accountService.getAccountById(senderId);
        Account reciever = accountService.getAccountById(recieverId);
        
        Transfer transfer = new Transfer(sender, reciever, amount);
        repo.persist(transfer);
        log.logger().log(Level.INFO, "Transfer from " + sender.getCustomer().getId() + " to " + reciever.getCustomer().getId() + " succeeded");
        return transfer;
    }
    
    public List<Transfer> getSendedTransfers(Account sender) {
        return repo.getBySender(sender);
    }
    
    public List<Transfer> getRecievedTransfers(Account reciever) {
        return repo.getByReciever(reciever);
    }
    
    public List<Transfer> getAllTransfers() {
        return repo.getAll();
    }
    
    public List<Transfer> getTransfersByAccount(Account account) {
        return repo.getTransfersByAccount(account);
    }
}
