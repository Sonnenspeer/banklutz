package de.othr.sw.banklutz.service;

import de.othr.sw.banklutz.annotation.CustomerBean;
import de.othr.sw.banklutz.annotation.EmployeeBean;
import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.CashTerminal;
import de.othr.sw.banklutz.entity.Customer;
import de.othr.sw.banklutz.entity.Employee;
import de.othr.sw.banklutz.entity.embeddable.Address;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

@RequestScoped
public class FixtureService implements Serializable {
    public static final String ADMIN_ID = "admin@sw.de";
    
    @Inject @EmployeeBean
    private EmployeeService employeeService;
    
    @Inject
    private CashTerminalService terminalService;
    
    @Inject @CustomerBean
    private CustomerService customerService;
    
    @Transactional(TxType.REQUIRES_NEW)
    public void generateFixtures() {
        generateAdmin();
        generateCustomer();
        generateTerminal();
    }
    
    private void generateAdmin() {
        Employee admin = employeeService.getEmployeeById(ADMIN_ID);
        
        if(admin == null) {
            Employee newAdmin = new Employee(ADMIN_ID, "sw#admin#1617");
            newAdmin.setAddress(new Address("Seybothstraße 2", "93059", "Regensburg"));
            newAdmin.setName("Admin");
            newAdmin.setLastName("OTH");

            employeeService.createEmployee(newAdmin);
        }
    }
    
    private void generateCustomer() {
        Customer customer = customerService.getCustomerById("max@mustermann.de");
        
        if(customer == null) {
            Customer newCustomer = new Customer("max@mustermann.de", "sw#admin#1617");
            newCustomer.setName("Max");
            newCustomer.setLastName("Mustermann");
            newCustomer.setAddress(new Address("Musterstraße 1", "12345", "Musterstadt"));

            Account account = new Account(1234);
            account.addCredits(100000);

            customerService.createCustomerWithAccount(newCustomer, account);
        }
    }
    
    private void generateTerminal() {
        CashTerminal terminal = terminalService.getTerminalById(2L);
        
        if(terminal == null) {
            CashTerminal newTerminal = new CashTerminal("Hauptautomat", 100000);
            
            terminalService.createCashTerminal(newTerminal);
        }
    }
}
