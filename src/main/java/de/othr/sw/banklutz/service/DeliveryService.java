package de.othr.sw.banklutz.service;

import de.othr.sw.banklutz.log.Log;
import de.othr.sw.lieferservice_roesch.service.Adresse;
import de.othr.sw.lieferservice_roesch.service.Auftrag;
import de.othr.sw.lieferservice_roesch.service.AuftragService;
import de.othr.sw.lieferservice_roesch.service.AuftragServiceService;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

@RequestScoped
public class DeliveryService {
    @Inject
    private Log log;
    
    public Auftrag createDelivery(int cent, Adresse from, Adresse to) {
        try { // Call Web Service Operation
            AuftragServiceService service = new AuftragServiceService();
            AuftragService port = service.getAuftragServicePort();
            // TODO initialize WS operation arguments here
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(new Date());
            XMLGregorianCalendar gregorianDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

            Auftrag arg0 = new Auftrag();
            arg0.setBetragInCent(cent);
            arg0.setVon(from);
            arg0.setNach(to);
            arg0.setErstelltAm(gregorianDate);
            // TODO process result here
            Auftrag result = port.createAuftrag(arg0);
            
            log.logger().log(Level.INFO, "Delivery created");
            
            return result;
        } catch (Exception ex) {
            log.logger().log(Level.SEVERE, "Delivery could not be created");
        }

        return null;
    }
    
    public List<Auftrag> getDeliveries() {
        try { // Call Web Service Operation
            AuftragServiceService service = new AuftragServiceService();
            AuftragService port = service.getAuftragServicePort();
            // TODO process result here
            List<Auftrag> result = port.getAllAuftrag();
          
            return result;
        } catch (Exception ex) {
            log.logger().log(Level.SEVERE, "Deliveries could not be get");
        }

        return null;
    }
}
