package de.othr.sw.banklutz.service;

import de.othr.sw.banklutz.annotation.EmployeeBean;
import de.othr.sw.banklutz.entity.Employee;
import de.othr.sw.banklutz.entity.User;
import de.othr.sw.banklutz.entity.repository.EmployeeRepository;
import de.othr.sw.banklutz.entity.util.EntityUtils;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@RequestScoped @EmployeeBean
public class EmployeeService extends UserService {
    @Inject
    private EmployeeRepository repo;
    
    @Transactional
    public Employee createEmployee(Employee employee) {      
        repo.persist(employee);
        return employee;
    }
    
    @Transactional
    public Employee updateEmployee(Employee employee) {
        employee = repo.merge(employee);
        return employee;
    }
    
    @Transactional
    public Employee deleteEmployee(Employee employee) {
        repo.remove(employee);
        return employee;
    }
    
    public List<Employee> getAllEmployees() {
        return repo.getAll();
    }
    
    public Employee getEmployeeById(String id) {
        return repo.getById(id);
    }
    
    public Employee checkUserPassword(String email, String password) {                 
        Employee u = repo.getById(email);
        if(u == null)
            return null;
        try {
            if( EntityUtils.hashPassword(password, u.getSalt(), User.HASH_ALGORITHM).equals(u.getPassword()) )
                return u;
            else 
                return null;
        } catch (EntityUtils.EntityUtilException ex) {
            throw new RuntimeException("password can not be hashed", ex);
        }
    }
}
