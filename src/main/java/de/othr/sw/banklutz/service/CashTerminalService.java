package de.othr.sw.banklutz.service;

import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.CashTerminal;
import de.othr.sw.banklutz.entity.repository.AccountRepository;
import de.othr.sw.banklutz.entity.repository.CashTerminalRepository;
import de.othr.sw.banklutz.exception.CreditOutOfLimitException;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@RequestScoped
public class CashTerminalService implements Serializable {
    @Inject
    private CashTerminalRepository repo;
    
    @Inject
    private AccountRepository repoAccount;
    
    @Transactional
    public CashTerminal createCashTerminal(CashTerminal terminal) {      
        repo.persist(terminal);
        return terminal;
    }
    
    @Transactional
    public CashTerminal updateCashTerminal(CashTerminal terminal) {
        terminal = repo.merge(terminal);
        return terminal;
    }
    
    @Transactional
    public CashTerminal deleteCashTerminal(CashTerminal terminal) {
        terminal = repo.merge(terminal);
        repo.remove(terminal);
        return terminal;
    }
    
    public List<CashTerminal> getAllTerminals() {
        return repo.getAll();
    }
    
    public CashTerminal getTerminalById(Long id) {
        return repo.getById(id);
    }
    
    @Transactional
    public void deposit(CashTerminal terminal, Account account, int amount) throws CreditOutOfLimitException {      
        terminal.reduceCash(amount);
        account.addCredits(amount);
        
        repo.merge(terminal);
        repoAccount.merge(account);
    }
    
    @Transactional
    public void withdraw(CashTerminal terminal, Account account, int amount) throws CreditOutOfLimitException {               
        account.reduceCredits(amount);
        terminal.addCash(amount);
        
        repo.merge(terminal);
        repoAccount.merge(account);
    }
}
