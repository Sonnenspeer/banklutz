package de.othr.sw.banklutz.service;

import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.Customer;
import de.othr.sw.banklutz.entity.repository.AccountRepository;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@RequestScoped
public class AccountService implements Serializable {
    @Inject
    private AccountRepository repo;
    
    @Transactional
    public Account addAccount(Customer customer, Account account) {
        repo.persist(account);
        customer.addAccount(account);
        return account;
    }
    
    @Transactional
    public Account updateAccount(Account account) {
        account = repo.merge(account);
        return account;
    }
    
    @Transactional
    public Account removeAccount(Customer customer, Account account) {
        customer.removeAccount(account);
        repo.remove(account);
        return account;
    }
    
    public Account getAccountById(Long id) {
        return repo.getById(id);
    }
    
    public boolean checkPin(Account account, int pin) {
        return account.getPin() == pin;
    }
}
