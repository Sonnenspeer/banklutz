package de.othr.sw.banklutz.service;

import de.othr.sw.banklutz.annotation.CustomerBean;
import de.othr.sw.banklutz.entity.Account;
import de.othr.sw.banklutz.entity.Customer;
import de.othr.sw.banklutz.entity.User;
import de.othr.sw.banklutz.entity.repository.AccountRepository;
import de.othr.sw.banklutz.entity.repository.CustomerRepository;
import de.othr.sw.banklutz.entity.util.EntityUtils;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@RequestScoped @CustomerBean
public class CustomerService extends UserService {
    @Inject
    private CustomerRepository repo; 
    
    @Inject
    private AccountRepository repoAccount;
    
    @Transactional
    public Customer createCustomer(Customer customer) {      
        repo.persist(customer);
        return customer;
    }
    
    @Transactional
    public Customer createCustomerWithAccount(Customer customer, Account account) { 
        accountService.addAccount(customer, account); 
        repo.persist(customer);
        return customer;
    }
    
    public Customer updateCustomer(Customer customer) {
        customer = repo.merge(customer);
        return customer;
    }
    
    @Transactional
    public Customer deleteCustomer(Customer customer) {
        customer = repo.merge(customer);
        repo.remove(customer);
        return customer;
    }
    
    public List<Customer> getAllCustomers() {
        return repo.getAll();
    }
    
    public Customer getCustomerById(String id) {
        return repo.getById(id);
    }
    
    public Customer checkUserPassword(String email, String password) {                 
        Customer u = repo.getById(email);
        if(u == null)
            return null;
        try {
            if( EntityUtils.hashPassword(password, u.getSalt(), User.HASH_ALGORITHM).equals(u.getPassword()) )
                return u;
            else 
                return null;
        } catch (EntityUtils.EntityUtilException ex) {
            throw new RuntimeException("password can not be hashed", ex);
        }
    }
}
