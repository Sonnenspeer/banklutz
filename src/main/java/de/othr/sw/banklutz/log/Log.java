package de.othr.sw.banklutz.log;

import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

@Dependent
public class Log {
    @Produces 
    @ApplicationScoped
    public Logger logger() {
        return Logger.getLogger("BankLutz");
    }
}
